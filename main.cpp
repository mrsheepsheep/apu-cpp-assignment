#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <set>

using namespace std;

#include "headers/UserManager.h"
#include "headers/User.h"
#include "headers/Storage.h"
#include "headers/Administrator.h"

using namespace SparePartManager;


int main() {
    while (true) {
        User *loggedUser = nullptr;
        printf("Welcome to SparePartManager. Please login with your username and password: \n");

        while (!loggedUser) {
            loggedUser = UserManager::login();
            if (!loggedUser) {
                printf("Invalid credentials. Please try again.\n");
            }
        }

        loggedUser->promptMenu();
        Storage::getInstance()->save();
        UserManager::getInstance()->save();

        printf("Would you like to quit ? ('yes'/'no'): ");
        string quit;
        getline(std::cin, quit);

        if (quit == "yes"){
            break;
        }
    }
}