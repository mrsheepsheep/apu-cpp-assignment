#ifndef SPAREPARTMANAGER_MENU_VIEWER_H
#define SPAREPARTMANAGER_MENU_VIEWER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <cassert>

namespace SparePartManager {
    class MenuViewer {
    public:
        virtual void promptMenu() = 0;
    };

}  // namespace SparePartManager
#endif
