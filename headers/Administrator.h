#ifndef SPAREPARTMANAGER_ADMINISTRATOR_H
#define SPAREPARTMANAGER_ADMINISTRATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <cassert>

#include "User.h"
#include "MenuViewer.h"
#include "SparePart.h"

namespace SparePartManager {
    class Administrator : public User {
    public:

        Administrator(std::string username, std::string password);

        bool isAdmin() override;

        void promptMenu() override;

        void searchPartsMenu();

        void editPartMenu(SparePart &part);

        void addPartMenu();

        void accountMenu();

        void listAccountsMenu();

        void newAccountMenu();

        void deleteAccountMenu();

        void changePasswordMenu();
    };

}  // namespace SparePartManager
#endif
