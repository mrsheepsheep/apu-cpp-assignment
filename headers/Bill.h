//
// Created by mrshe on 04/05/2019.
//

#ifndef CPPASSIGNMENT_BILL_H
#define CPPASSIGNMENT_BILL_H

#include <vector>

#include "User.h"
#include "SparePart.h"
#include "BillItem.h"


namespace SparePartManager {
    class Bill {

    private:
        User *user;
        std::vector<BillItem> parts;

    public:

        explicit Bill(User *user);

        std::vector<BillItem> getItems();

        User *getUser();

        void addPart(SparePart *part, int number);

        void delPart(int id);

        void save();
    };

}

#endif //CPPASSIGNMENT_BILL_H
