//
// Created by mrshe on 04/05/2019.
//

#ifndef CPPASSIGNMENT_USERMANAGER_H
#define CPPASSIGNMENT_USERMANAGER_H

#include "User.h"

namespace SparePartManager {

    class UserManager {

    private:
        static UserManager *instance;
        std::vector<User*> users;
    public:
        UserManager();
        ~UserManager();
        
        static UserManager *getInstance();

        std::vector<User*> getUsers();

        void load();

        void save();

        void addUser(User *user);

        void delUser(User *user);

        /* Prompts the user and login */
        static User *login();

        /* Login with given username and password */
        static User *login(std::string username, std::string password);

    };

}

#endif //CPPASSIGNMENT_USERMANAGER_H
