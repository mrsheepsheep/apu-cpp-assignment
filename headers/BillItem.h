//
// Created by mrshe on 04/05/2019.
//

#ifndef CPPASSIGNMENT_BILLITEM_H
#define CPPASSIGNMENT_BILLITEM_H

#include "SparePart.h"

namespace SparePartManager {

    class BillItem {
    private:
        SparePart *part;
        int quantity;
    public:
        BillItem(SparePart *part, int quantity);

        SparePart *getPart();

        int getQuantity();

        std::string toString();
    };
}

#endif //CPPASSIGNMENT_BILLITEM_H
