#ifndef SPAREPARTMANAGER_STORAGE_H
#define SPAREPARTMANAGER_STORAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <cassert>

#include "SparePart.h"

namespace SparePartManager {
    class Storage {
    private:

        Storage();

        ~Storage();

        static Storage *instance;

        // Very important: Store pointers to SpareParts in order to modify them on the go !
        std::vector<SparePart *> storage;

    public:
        static Storage *getInstance();

        std::vector<SparePart *> &getParts();

        void addPart(SparePart *part);

        void deletePart(SparePart *part);

        void save();

        void load();

    };

}  // namespace SparePartManager
#endif
