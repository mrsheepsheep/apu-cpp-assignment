#ifndef SPAREPARTMANAGER_SPARE_PART_H
#define SPAREPARTMANAGER_SPARE_PART_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <cassert>

namespace SparePartManager {
    class SparePart {
    private:
        int id;

        int supplies;

        double price;

        std::string name;

        std::string manufacturer;

        std::string dateOfManufacture;

        static const char SEPARATOR = ';';

    public:

        SparePart(int id, std::string name, std::string manufacturer, std::string dateOfManufacture, double price,
                  int supplies);

        int getId();

        void setId(int id);

        int getSupplies();

        void setSupplies(int supplies);

        double getPrice();

        void setPrice(double price);

        std::string getName();

        void setName(std::string name);

        std::string getManufacturer();

        void setManufacturer(std::string manufacturer);

        std::string getDateOfManufacture();

        void setDateOfManufacture(std::string date);

        std::string toString();

        static SparePart *fromString(std::string string);

    };

}  // namespace SparePartManager
#endif
