#ifndef SPAREPARTMANAGER_CUSTOMER_H
#define SPAREPARTMANAGER_CUSTOMER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <cassert>

#include "MenuViewer.h"
#include "User.h"
#include "SparePart.h"
#include "Bill.h"

namespace SparePartManager {
    class Customer : public User {
    private:

        Bill *bill;

    public:

        Bill *getBill();

        Customer(std::string username, std::string password);

        bool isAdmin() override;

        void promptMenu() override;

        void searchPartsMenu();

        void showPartMenu(SparePart &part);

        void billMenu();

        void logout() override;

    };

}  // namespace SparePartManager
#endif
