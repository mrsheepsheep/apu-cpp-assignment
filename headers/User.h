#ifndef SPAREPARTMANAGER_USER_H
#define SPAREPARTMANAGER_USER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <cassert>
#include "MenuViewer.h"

namespace SparePartManager {
    class User : public MenuViewer {
    private:
        std::string username;
        std::string password;

    public:
        User(std::string username, std::string password);

        std::string getUsername();

        void setUsername(std::string username);

        std::string getPassword();

        void setPassword(std::string password);

        virtual void logout();

        virtual bool isAdmin() = 0;

        std::string toString();

    };

}  // namespace SparePartManager
#endif