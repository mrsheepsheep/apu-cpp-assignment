//
// Created by mrshe on 04/05/2019.
//

#include <fstream>
#include <cstring>
#include "../headers/Bill.h"

namespace SparePartManager {

    Bill::Bill(User *user) {
        this->user = user;
    }

    std::vector<BillItem> Bill::getItems() {
        return parts;
    }

    User *Bill::getUser() {
        return this->user;
    }

    void Bill::addPart(SparePart *part, int number) {
        BillItem item{part, number};
        parts.push_back(item);
    }

    void Bill::delPart(int id) {
        parts.erase(parts.begin() + id);
    }

    void Bill::save() {
        std::ofstream f;
        f.open("bills.csv", std::ios::out | std::ios::app);
        if (f.is_open()) {
            double total = 0;
            f << "\n";
            f << "BILL FOR " + getUser()->getUsername() << "\n";
            f << "Part ID;Part name;Manufacturer;Date of Manufacture; Price /u; Supplies; Quantity requested; Total price;\n";
            for (BillItem item : parts) {
                f << item.toString() << ";" << std::to_string(item.getPart()->getPrice() * item.getQuantity()) << "\n";
                total += item.getPart()->getPrice() * item.getQuantity();
            }
            f << ";;;;;;Total;"<< std::to_string(total) <<";\n";
        } else {
            printf("Error: %s\n", strerror(errno));
        }
        f.close();
    }
}