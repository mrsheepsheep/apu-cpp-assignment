//
// Created by mrshe on 04/05/2019.
//

#include "../headers/BillItem.h"

namespace SparePartManager {

    BillItem::BillItem(SparePart *part, int quantity) {
        this->part = part;
        this->quantity = quantity;
    }

    SparePart *BillItem::getPart() {
        return part;
    }

    int BillItem::getQuantity() {
        return quantity;
    }

    std::string BillItem::toString() {
        return part->toString() + std::to_string(quantity);
    }
}