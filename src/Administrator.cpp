#include <utility>

#include <string>
#include <vector>
#include <list>
#include <iostream>

#include "../headers/Administrator.h"
#include "../headers/Storage.h"
#include "../headers/UserManager.h"
#include "../headers/Customer.h"

namespace SparePartManager {

    Administrator::Administrator(std::string username, std::string password) : User(username, password) {

    }

    bool Administrator::isAdmin() {
        return true;
    }

    void Administrator::promptMenu() {

        do {
            printf("Welcome, %s", getUsername().c_str());
            printf("\n\n== Administrator Menu ==\n");
            printf("'search': Search & Edit SpareParts\n");
            printf("'add': Add a new SparePart\n");
            printf("'account': Account Management\n");
            printf("'logout': Logout\n");

            printf("Please select an action: ");
            std::string choice;
            std::getline(std::cin, choice);

            if (choice == "search")
                searchPartsMenu();
            else if (choice == "add")
                addPartMenu();
            else if (choice == "account")
                accountMenu();
            else if (choice == "logout") {
                logout();
                break;
            } else {
                printf("Unknown action.\n");
            }
        } while (true);
    }

    void Administrator::searchPartsMenu() {
        printf("\n\n== Search Parts Menu ==\n");
        Storage *storage = Storage::getInstance();

        printf("Available parts:\n");
        printf("\n%5s | %10s | %5s | %8s | %12s | %10s \n\n", "ID", "NAME", "PRICE", "SUPPLIES", "MANUFACTURER",
               "DATE");
        for (SparePart *part : storage->getParts()) {
            printf("%5.5s | %10.10s | %5.5s | %8.8s | %12.12s | %10.10s \n",
                   std::to_string(part->getId()).c_str(),
                   part->getName().c_str(),
                   std::to_string(part->getPrice()).c_str(),
                   std::to_string(part->getSupplies()).c_str(), part->getManufacturer().c_str(),
                   part->getDateOfManufacture().c_str());
        }
        printf("\n");

        do {
            printf("Input the part ID you want to edit. Type 'q' to quit.\n");
            printf("Part ID: ");

            std::string search;
            std::getline(std::cin, search);

            if (search == "q") {
                break;
            }

            try {
                int searchId = std::stoi(search);
                // Find corresponding SparePart
                SparePart *found = nullptr;
                for (SparePart *part : storage->getParts()) {
                    if (part->getId() == searchId) {
                        found = part;
                        break;
                    }
                }
                if (found == nullptr) {
                    printf("Could not find this ID. Please try again.\n");
                } else {
                    editPartMenu(*found);
                    break;
                }
            } catch (...) {
                printf("You must input a number.\n");
            }
        } while (true);
    }

    void Administrator::editPartMenu(SparePart &part) {
        do {
            printf("\n\n== Edit Spare Part Menu ==\n");
            printf("%-20s: %d\n", "ID ", part.getId());
            printf("%-20s: %s\n", "Name", part.getName().c_str());
            printf("%-20s: $%s\n", "Price", std::to_string(part.getPrice()).c_str());
            printf("%-20s: %s units\n", "Supplies", std::to_string(part.getSupplies()).c_str());
            printf("%-20s: %s\n", "Manufacturer", part.getManufacturer().c_str());
            printf("%-20s: %s\n", "Date of Manufacture", part.getDateOfManufacture().c_str());
            printf("===========================\n");
            printf("Available actions:\n");
            printf("'id': Set ID\n");
            printf("'name': Set Name\n");
            printf("'price': Set Price\n");
            printf("'supplies': Set Supplies\n");
            printf("'manufacturer': Set Manufacturer\n");
            printf("'date': Set Date of manufacture\n");
            printf("'delete': Delete\n");
            printf("'quit': Go back to Administration Menu\n");

            printf("Please select an action: ");
            std::string search;
            std::getline(std::cin, search);

            if (search == "id") {
                printf("Please input the new ID: ");
                std::string id;
                std::getline(std::cin, id);
                try {
                    int change = std::stoi(id);
                    bool exists = false;
                    // Check that the ID does not exist already
                    for (SparePart *lpart : Storage::getInstance()->getParts()) {
                        if (lpart->getId() == change) {
                            exists = true;
                        }
                    }
                    if (exists) {
                        printf("This ID already exists.\n");
                    } else {
                        part.setId(change);
                        printf("ID changed successfully.\n");
                    }
                } catch (...) {
                    printf("Please input a number.\n");
                }
            } else if (search == "name") {
                printf("Please input the new name: ");
                std::string name;
                std::getline(std::cin, name);
                part.setName(name);
                printf("Name successfully changed.\n");
            } else if (search == "price") {
                printf("Please input the new price: ");
                std::string priceString;
                std::getline(std::cin, priceString);
                try {
                    double price = std::stod(priceString);
                    part.setPrice(price);
                    printf("Price changed successfully.\n");
                } catch (...) {
                    printf("Please input a number\n");
                }
            } else if (search == "supplies") {
                printf("Please input the number of available supplies: ");
                std::string suppliesString;
                std::getline(std::cin, suppliesString);
                try {
                    int supplies = std::stoi(suppliesString);
                    part.setSupplies(supplies);
                    printf("Supplies set successfully.\n");
                } catch (...) {
                    printf("Please input a number.\n");
                }
            } else if (search == "manufacturer") {
                printf("Please input the new manufacturer: ");
                std::string manufacturer;
                std::getline(std::cin, manufacturer);
                part.setManufacturer(manufacturer);
                printf("Manufacturer successfully changed.\n");
            } else if (search == "date") {
                printf("Please input the new date: ");
                std::string date;
                std::getline(std::cin, date);
                part.setDateOfManufacture(date);
                printf("Date successfully changed.\n");
            } else if (search == "delete") {
                printf("Please confirm the deletion of this part. Type 'yes': ");
                std::string yes;
                std::getline(std::cin, yes);
                if (yes == "yes") {
                    printf("This part has been deleted.\n");
                    Storage::getInstance()->deletePart(&part);
                    break;
                } else {
                    printf("Deletion cancelled.\n");
                }
            } else if (search == "quit") {
                break;
            } else {
                printf("Unknown action.\n");
            }

        } while (true);
    }

    void Administrator::addPartMenu() {
        printf("\n\n== New Part Menu ==\n");
        printf("At any time, type 'q' to abort part creation.\n");

        try {
            printf("Part ID: ");
            std::string idString;
            std::getline(std::cin, idString);
            if (idString == "q") {
                return;
            }
            int id = std::stoi(idString);

            printf("Name: ");
            std::string name;
            std::getline(std::cin, name);
            if (name == "q") {
                return;
            }

            printf("Price: ");
            std::string priceString;
            std::getline(std::cin, priceString);
            if (idString == "q") {
                return;
            }
            double price = std::stod(priceString);

            printf("Available supplies: ");
            std::string suppliesString;
            std::getline(std::cin, suppliesString);
            if (suppliesString == "q") {
                return;
            }
            int supplies = std::stoi(suppliesString);

            printf("Manufacturer: ");
            std::string manufacturer;
            std::getline(std::cin, manufacturer);
            if (manufacturer == "q") {
                return;
            }

            printf("Manufacturing date: ");
            std::string date;
            std::getline(std::cin, date);
            if (date == "q") {
                return;
            }

            printf("Saving the new part...\n");
            SparePart *part = new SparePart(id, name, manufacturer, date, price, supplies);
            Storage::getInstance()->addPart(part);
            Storage::getInstance()->save();
            printf("Part successfully added !\n");

        } catch (...) {
            printf("An error occurred.\n");
        }

    }

    void Administrator::accountMenu() {
        do {
            printf("\n\n== Account Management Menu ==\n");
            printf("'list': List available accounts\n");
            printf("'new': New account\n");
            printf("'delete': Delete an account\n");
            printf("'password': Change an account's password\n");
            printf("'quit': Go back to Administration Menu\n");
            printf("Please select an action: ");
            std::string choice;
            std::getline(std::cin, choice);

            if (choice == "list") {
                listAccountsMenu();
            } else if (choice == "new") {
                newAccountMenu();
            } else if (choice == "delete") {
                deleteAccountMenu();
            } else if (choice == "password") {
                changePasswordMenu();
            } else if (choice == "quit") {
                break;
            } else {
                printf("Unknown action.\n");
            }
        } while (true);
    }

    void Administrator::listAccountsMenu() {
        printf("\n\n== Accounts List ==\n");
        printf("%10s | %20s | %5s\n\n", "username", "password", "admin");
        for (User *user : UserManager::getInstance()->getUsers()) {
            printf("%10.10s | %20.20s | %5.5s\n", user->getUsername().c_str(), user->getPassword().c_str(),
                   (user->isAdmin() ? "*" : ""));
        }
    }

    void Administrator::newAccountMenu() {
        printf("\n\n== New Account ==\n");
        printf("Enter 'q' to quit.\n");
        std::string user;
        bool valid = false;
        while (!valid) {
            printf("Username: ");
            getline(std::cin, user);

            if (user == "q") {
                return;
            } else {
                valid = true;
                for (User *u : UserManager::getInstance()->getUsers()) {
                    if (u->getUsername() == user) {
                        printf("This username is already in use. Please choose another one.\n");
                        valid = false;
                        break;
                    }
                }
            }
        }

        printf("Password: ");
        std::string password;
        getline(std::cin, password);

        printf("If this user is an administrator, type 'administrator', otherwise type 'no'.\n");
        printf("Administrator ? ");
        std::string adminString;
        getline(std::cin, adminString);

        if (adminString == "administrator") {
            UserManager::getInstance()->addUser(new Administrator(user, password));
        } else {
            UserManager::getInstance()->addUser(new Customer(user, password));
        }

        printf("Account successfully created !");
    }

    void Administrator::deleteAccountMenu() {
        printf("\n\n== Account Deletion ==\n");
        printf("Type 'q' to cancel account deletion.\n");
        User *found = nullptr;
        while (!found) {
            printf("Please enter the username you wish to delete: ");
            std::string username;
            getline(std::cin, username);

            if (username == "q") {
                return;
            }
            if (username == getUsername()) {
                printf("You cannot delete your own account !\n");
            } else {

                for (User *u : UserManager::getInstance()->getUsers()) {
                    if (u->getUsername() == username) {
                        found = u;
                        break;
                    }
                }

                if (found) {
                    UserManager::getInstance()->delUser(found);
                    printf("Account deleted.\n");
                } else {
                    printf("This username does not exist.\n");
                }
            }
        }
    }

    void Administrator::changePasswordMenu() {
        printf("\n\n== Password Change Menu ==\n");
        printf("Type 'q' to cancel password change.\n");
        User *found = nullptr;
        while (!found) {
            printf("Please enter the username you wish to change the password of: ");
            std::string username;
            getline(std::cin, username);

            if (username == "q") {
                return;
            }
            for (User *u : UserManager::getInstance()->getUsers()) {
                if (u->getUsername() == username) {
                    found = u;
                    break;
                }
            }

            if (found) {
                printf("Please carefully choose the new password: ");
                std::string password;
                getline(std::cin, password);
                found->setPassword(password);
                printf("Password changed successfully.\n");
            } else {
                printf("This username does not exist.\n");
            }
        }
    }


}  // namespace SparePartManager