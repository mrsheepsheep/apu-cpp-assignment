#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>

#include "../headers/User.h"
#include "../headers/Administrator.h"
#include "../headers/Customer.h"

namespace SparePartManager {

    User::User(std::string username, std::string password) : MenuViewer() {
        this->username = username;
        this->password = password;
    }

    void User::logout() {
        printf("Exiting...\n");
    }

    std::string User::getUsername() {
        return username;
    }

    void User::setUsername(std::string username) {
        this->username = username;
    }

    std::string User::getPassword() {
        return password;
    }

    void User::setPassword(std::string password) {
        this->password = password;
    }

    std::string User::toString() {
        return getUsername() + ";" + getPassword() + ";" +(isAdmin() ? "admin" : "") + ";";
    }


}  // namespace SparePartManager