#include <utility>

#include <utility>

#include <utility>

#include <string>
#include <vector>
#include <list>
#include <iostream>

#include "../headers/Customer.h"
#include "../headers/Storage.h"

namespace SparePartManager {

    Customer::Customer(std::string username, std::string password) : User(username, password) {
        bill = new Bill(this);
    }

    bool Customer::isAdmin() {
        return false;
    }

    Bill *Customer::getBill() {
        return this->bill;
    }

    void Customer::promptMenu() {
        bool repeat;
        do {
            repeat = true;
            printf("\n== Customer Menu ==\n");
            printf("'search': Search & Buy SpareParts\n");
            printf("'bill': Show your current bill\n");
            printf("'logout': Logout\n");

            printf("Please select an action: ");
            std::string choice;
            std::getline(std::cin, choice);

            if (choice == "search")
                searchPartsMenu();
            else if (choice == "bill")
                billMenu();
            else if (choice == "logout") {
                logout();
                repeat = false;
            } else {
                printf("Unknown action.\n");
            }
        } while (repeat);
    }

    void Customer::searchPartsMenu() {
        printf("\n== Search Parts Menu ==\n");
        Storage *storage = Storage::getInstance();

        printf("Available parts:\n");
        printf("\n%5s | %10s | %5s | %8s | %12s | %10s \n\n", "ID", "NAME", "PRICE", "SUPPLIES", "MANUFACTURER",
               "DATE");
        for (SparePart *part : storage->getParts()) {
            printf("%5.5s | %10.10s | %5.5s | %8.8s | %12.12s | %10.10s \n",
                   std::to_string(part->getId()).c_str(),
                   part->getName().c_str(),
                   std::to_string(part->getPrice()).c_str(),
                   std::to_string(part->getSupplies()).c_str(), part->getManufacturer().c_str(),
                   part->getDateOfManufacture().c_str());
        }
        printf("\n");

        bool repeat;
        do {
            repeat = true;
            printf("Input the part ID you want information about. Type 'q' to quit.\n");
            printf("Part ID: ");

            std::string search;
            std::getline(std::cin, search);

            if (search == "q") {
                break;
            }

            try {
                int searchId = std::stoi(search);
                // Find corresponding SparePart
                SparePart *found = nullptr;
                for (SparePart *part : storage->getParts()) {
                    if (part->getId() == searchId) {
                        found = part;
                        break;
                    }
                }
                if (found == nullptr) {
                    printf("Could not find this ID. Please try again.\n");
                } else {
                    // Check this item is not already billed
                    bool billed = false;
                    for (BillItem item : getBill()->getItems()) {
                        if (item.getPart()->getId() == found->getId()) {
                            billed = true;
                            break;
                        }
                    }
                    if (billed) {
                        printf("This item is already billed. Delete it from your bill first.\n");
                    } else {
                        showPartMenu(*found);
                        break;
                    }
                }
            } catch (...) {
                printf("You must input a number.\n");
            }
        } while (repeat);

    }

    void Customer::showPartMenu(SparePart &part) {
        printf("\n== Spare Part Information ==\n");
        printf("%-20s: %d\n", "ID ", part.getId());
        printf("%-20s: %s\n", "Name", part.getName().c_str());
        printf("%-20s: $%f\n", "Price", part.getPrice());
        printf("%-20s: %d units\n", "Supplies", part.getSupplies());
        printf("%-20s: %s\n", "Manufacturer", part.getManufacturer().c_str());
        printf("%-20s: %s\n", "Date of Manufacture", part.getDateOfManufacture().c_str());
        printf("==============================\n");

        bool invalid;
        do {
            invalid = true;
            printf("Please enter the number of parts you want to buy.\n");
            printf("Enter 0 to cancel.\n");
            printf("Number of parts: ");
            std::string numberString;
            std::getline(std::cin, numberString);
            try {
                int number = std::stoi(numberString);
                if (number == 0) {
                    break;
                }
                if (number < 0 || number > part.getSupplies()) {
                    printf("There are not enough supplies !\n");
                } else {
                    invalid = false;

                    this->getBill()->addPart(&part, number);

                    printf("Part added to your cart.\n");
                }
            } catch (...) {
                printf("Please input an number.\n");
            }
        } while (invalid);
    }

    void Customer::billMenu() {
        double total = 0;
        int no = 0;

        printf("\n== Bill Menu ==\n");
        printf("\n %3s | %5s | %10s | %7s | %8s | %10s \n\n", "N°", "ID", "NAME", "PRICE/u", "QUANTITY", "TOTAL");
        for (BillItem item : bill->getItems()) {
            SparePart *part = item.getPart();
            total += item.getQuantity() * part->getPrice();
            printf("%3s | %5.5s | %10.10s | %7.7s | %8.8s | %10.10s \n",
                   std::to_string(no).c_str(),
                   std::to_string(part->getId()).c_str(),
                   part->getName().c_str(),
                   std::to_string(part->getPrice()).c_str(),
                   std::to_string(item.getQuantity()).c_str(),
                   std::to_string(item.getQuantity() * part->getPrice()).c_str());
            no++;
        }
        printf("\n");
        printf("%58s\n\n", (std::string("TOTAL ") + std::to_string(total)).c_str());

        printf("Note: This bill must be validated by an administrator before being processed.\n");
        printf("If you wish to remove an item, please enter the line number. Type 'q' to quit.\n");
        printf("Line number to remove: ");

        std::string lineString;
        std::getline(std::cin, lineString);

        if (lineString == "q") {
            return;
        }

        try {
            int line = std::stoi(lineString);
            if (line >= 0 && line <= getBill()->getItems().size()) {
                getBill()->delPart(line);
                printf("Line deleted.\n");
            } else {
                printf("This line does not exist.\n");
            }
        } catch (...) {
            printf("Please input a valid line number to remove or 'q' to quit.\n");
        }
    }

    void Customer::logout() {
        if (!bill->getItems().empty()) {
            printf("Saving Bill...\n");
            getBill()->save();
            printf("Bill saved.\n");
        }
        User::logout();
    }

}  // namespace SparePartManager
