#include <string>
#include <vector>

#include "../headers/SparePart.h"

namespace SparePartManager {

    SparePart::SparePart(int id, std::string name, std::string manufacturer, std::string dateOfManufacture,
                         double price, int supplies) {
        this->id = id;
        this->name = std::move(name);
        this->manufacturer = std::move(manufacturer);
        this->dateOfManufacture = std::move(dateOfManufacture);
        this->price = price;
        this->supplies = supplies;
    }

    int SparePart::getId() {
        return id;
    }

    void SparePart::setId(int id) {
        this->id = id;
    }

    int SparePart::getSupplies() {
        return this->supplies;
    }

    void SparePart::setSupplies(int supplies) {
        this->supplies = supplies;
    }

    double SparePart::getPrice() {
        return price;
    }

    void SparePart::setPrice(double price) {
        this->price = price;
    }

    std::string SparePart::getName() {
        return name;
    }

    void SparePart::setName(std::string name) {
        this->name = std::move(name);
    }

    std::string SparePart::getManufacturer() {
        return manufacturer;
    }

    void SparePart::setManufacturer(std::string manufacturer) {
        this->manufacturer = std::move(manufacturer);
    }

    std::string SparePart::getDateOfManufacture() {
        return dateOfManufacture;
    }

    void SparePart::setDateOfManufacture(std::string date) {
        this->dateOfManufacture = std::move(date);
    }

    std::string SparePart::toString() {
        return std::to_string(id) + SEPARATOR + name + SEPARATOR + manufacturer + SEPARATOR + dateOfManufacture +
               SEPARATOR + std::to_string(price) + SEPARATOR + std::to_string(supplies) + SEPARATOR;
    }

    SparePart *SparePart::fromString(std::string s) {
        int id = -1;
        std::string name;
        std::string manufacturer;
        std::string dateOfManufacture;
        double price = 0;
        int supplies = 0;

        int state = 0;
        std::string word;
        for (int n = 0; n < s.size(); n++) {
            char i = s[n];
            if (i != SEPARATOR && n != s.size()-1) {
                word += i;
            } else {
                switch (state) {
                    case 0:
                        id = std::atoi(word.c_str());
                        break;
                    case 1:
                        name = word;
                        break;
                    case 2:
                        manufacturer = word;
                        break;
                    case 3:
                        dateOfManufacture = word;
                        break;
                    case 4:
                        price = std::atof(word.c_str());
                        break;
                    case 5:
                        supplies = std::atoi(word.c_str());
                        break;
                    default:
                        return new SparePart(-1, "undefined", "undefined", "undefined", 0, 0);
                }
                state++;
                word = "";
            }
        }
        return new SparePart(id, name, manufacturer, dateOfManufacture, price, supplies);

    }
}  // namespace SparePartManager
