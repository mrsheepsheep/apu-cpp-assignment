//
// Created by mrshe on 04/05/2019.
//

#include <fstream>
#include <cstring>
#include "../headers/UserManager.h"
#include "../headers/Administrator.h"
#include "../headers/Customer.h"

namespace SparePartManager {

    UserManager *UserManager::instance = nullptr;

    UserManager::UserManager() {
        load();
    }

    UserManager::~UserManager() {
        save();
    }

    UserManager *UserManager::getInstance() {
        if (!UserManager::instance) {
            instance = new UserManager;
        }
        return instance;
    }

    std::vector<User *> UserManager::getUsers() {
        return users;
    }


    void UserManager::load() {
        printf("Loading users...\n");

        std::ifstream z;
        z.open("users.csv", std::ios::binary);
        if (z.is_open()) {
            std::string line;
            while (getline(z, line)) {
                std::string username_read;
                std::string password_read;
                bool admin = false;

                int state = 0;
                std::string word;
                for (int n = 0; n < line.size(); n++) {
                    char i = line[n];
                    if (i != ';' && n != line.size() - 1) {
                        word += i;
                    } else {
                        switch (state) {
                            case 0:
                                username_read = word;
                                break;
                            case 1:
                                password_read = word;
                                break;
                            case 2:
                                admin = word == "admin";
                                break;
                            default:
                                break;
                        }
                        state++;
                        word = "";
                    }
                }
                User *user = nullptr;
                if (admin) {
                    user = new Administrator(username_read, password_read);
                } else {
                    user = new Customer(username_read, password_read);
                }
                addUser(user);
            }
        } else {
            printf("Error: %s\n", strerror(errno));
        }
        printf("Finished loading users.\n");
    }

    void UserManager::save() {
        printf("Saving users...\n");

        std::ofstream f;
        f.open("users.csv", std::ios::out | std::ios::trunc);
        if (f.is_open()) {
            for (User *user : users) {
                f << user->toString() << "\n";
            }
        } else {
            printf("Error: %s\n", strerror(errno));
        }

        printf("Users saved.\n");
    }

    void UserManager::addUser(User *user) {
        users.push_back(user);
    }

    void UserManager::delUser(User *user) {
        int i;
        for (i = 0; i < users.size(); i++) {
            if (users[i] == user) {
                break;
            }
        }
        users.erase(users.begin() + i);
    }

    User *UserManager::login() {
        std::string username, password;
        printf("Username: ");
        std::getline(std::cin, username);
        printf("Password: ");
        std::getline(std::cin, password);

        return UserManager::login(username, password);
    }

    User *UserManager::login(std::string username, std::string password) {
        for (User *u : getInstance()->getUsers()) {
            if (u->getUsername() == username && u->getPassword() == password) {
                return u;
            }
        }
        return nullptr;
    }

}