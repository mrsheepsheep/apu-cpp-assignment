#include <string>
#include <vector>
#include <iostream>
#include <set>
#include <fstream>
#include <cstring>

#include "../headers/Storage.h"

namespace SparePartManager {
    Storage *Storage::instance = nullptr;

    Storage::Storage() {
        load();
    }

    Storage::~Storage() {
        save();
    }

    Storage *Storage::getInstance() {
        if (!Storage::instance) {
            instance = new Storage;
        }
        return instance;
    }

    std::vector<SparePart *> &Storage::getParts() {
        return storage;
    }

    void Storage::addPart(SparePart *part) {
        storage.push_back(part);
    }

    void Storage::deletePart(SparePart *part) {
        int i;
        for (i = 0; i < storage.size(); i++) {
            if (storage[i] == part) {
                break;
            }
        }
        storage.erase(storage.begin() + i);
        delete part;
    }

    void Storage::save() {
        std::set<int> ids; // Store unique ids

        printf("Saving storage...\n");
        std::ofstream f;
        f.open("storage.csv", std::ios::out | std::ios::trunc);
        if (f.is_open()) {
            /* OLD UNIQUE ID VERSION */
            /*
            while (!ids.insert(part->getId()).second) {
                // ID already exists. Prompt for modification.
                std::cout << "Error while saving " << part->getName() << " (ID " << part->getId() << ")\n";
                std::cout << "This ID already exists. Please input another one:";
                int id = -1;

                std::string idString;
                getline(std::cin, idString);

                try {
                    int id = std::stoi(idString);
                    part->setId(id);
                } catch (...){
                    printf("Please input a number.");
                }
            };
            */

            for (SparePart *part : storage) {
                f << part->toString() << "\n";
            }
        } else {
            printf("Error: %s\n", strerror(errno));
        }
        f.close();
        printf("Storage saved\n");
    }

    void Storage::load() {

        int debug = 0;
        printf("Loading storage...\n");
        std::ifstream z;
        z.open("storage.csv");
        if (z.is_open()) {
            std::string line;
            while (getline(z, line)) {
                SparePart *part = SparePart::fromString(line);
                // Only valid parts have id >= 0
                if (part->getId() >= 0) {
                    addPart(part);
                } else {
                    printf("Invalid part. Ignoring... (%s)\n", line.c_str());
                }
            }
        } else {
            printf("Error: %s\n", strerror(errno));
        }
        z.close();
        printf("Storage loaded.\n");
    }
}  // namespace SparePartManager
